#!/bin/sh

set -eu

BIN_DIR=~/binaries
IMAGE_DIR=/release/images
HOME_DIR=/etc/home
trap cleanup EXIT ERR

verify_user() {
    if [ $(id -u) -eq 0 ]; then
        log "Must not be run as root"
        exit 1
    fi
}

log() {
    echo "*** $1"
}

install_jq() {
    if command -v jq > /dev/null; then
    true
    else
    log "Installing jq"
        doas pkg_add jq
    fi
}

enable_doas() {
    log "Enabling doas..."
    echo 'permit nopass setenv { ENV PS1 SSH_AUTH_SOCK } :wheel' | doas tee -a "${DIR}"/fs/etc/doas.conf > /dev/null
}

setup_network() {
    local NETWORK_INTERFACE="$1"
    local NETWORK_CONF="$2"
    log "Setting up network ${NETWORK_INTERFACE}"
    doas touch "${DIR}/fs/etc/hostname.${NETWORK_INTERFACE}"
    doas chown root:wheel "${DIR}/fs/etc/hostname.${NETWORK_INTERFACE}"
    echo "${NETWORK_CONF}" | doas tee -a "${DIR}/fs/etc/hostname.${NETWORK_INTERFACE}" > /dev/null
}

move_root_home() {
    if doas test -d "${DIR}/fs/${HOME_DIR}/root"; then
        log "Root account seems already moved"
    else
        log "Moving home for root to ${HOME_DIR}..."
        doas mkdir -p "${DIR}/fs/${HOME_DIR}"
        doas chroot "${DIR}/fs" usermod -d "${HOME_DIR}/root" -m root
    fi
}

set_hostname() {
    local domain="$1"

    echo "router.${domain}" | doas tee -a ${DIR}/fs/etc/myname > /dev/null
    echo "127.0.0.1 localhost localhost.localdomain localhost4 localhost4.localdomain4 router router.${domain}" | doas tee -a ${DIR}/fs/etc/hosts > /dev/null
    echo "::1 localhost localhost localhost.localdomain localhost6 localhost6.localdomain6 router router.${domain}" | doas tee -a ${DIR}/fs/etc/hosts > /dev/null
}

add_user() {
    local ADDITIONAL_USER="$1"

    local DEST="${HOME_DIR}/${ADDITIONAL_USER}"
    if doas test -d "${DIR}/fs/${DEST}"; then
    log "User account seems already created"
    else
    log "Creating user ${ADDITIONAL_USER}..."
        doas chroot "${DIR}/fs" useradd -p "" -d ${DEST} -m -G wheel ${ADDITIONAL_USER}
    fi
}

install_authorized_keys() {
    local ADDITIONAL_USER=$1
    local SSH_PUBKEYS="$2"

    for user in root "${ADDITIONAL_USER}"; do
        if doas test ! -d "${DIR}/fs/etc/home/${user}/.ssh"; then
            doas mkdir "${DIR}/fs/etc/home/${user}/.ssh"
            doas chmod 700 "${DIR}/fs/etc/home/${user}/.ssh"
        fi
        doas rm -f "${DIR}/fs/etc/home/${user}/.ssh/authorized_keys"
        doas touch "${DIR}/fs/etc/home/${user}/.ssh/authorized_keys"
        doas chmod 600 "${DIR}/fs/etc/home/${user}/.ssh/authorized_keys"
        doas chown "${user}" "${DIR}/fs/etc/home/${user}/.ssh/authorized_keys"
        echo "${SSH_PUBKEYS}"| doas tee -a "${DIR}/fs/etc/home/${user}/.ssh/authorized_keys" > /dev/null
    done
}

set_password() {
    local user=$1
    local hash=$2

    log "Setting password for user ${user}"
    hash_length=$(echo -n ${hash} | wc -m)
    if [ ${hash_length} -lt 20 ]; then
        echo "Illegal length of hash"
        exit 1
    fi

    doas chroot ${DIR}/fs usermod -p "${hash}" "${user}"
}

copy_image() {
    local ORIG=$1
    local HOST_IMAGE=$2

    log "Copying (and overwriting) ${ORIG} -> ${HOST_IMAGE}"
    doas rm -f ${HOST_IMAGE}
    doas cp -av ${ORIG} ${HOST_IMAGE}
}

mount_image() {
    local IMAGE=$1
    
    log "Mounting image ${IMAGE}..."
    DIR=$(doas /root/resflash/mount_resflash.sh ${IMAGE})
}

mount_fs() {
    local FS=$1

    log "Mounting fs ${FS}..."
    DIR=$(doas /root/resflash/mount_resflash.sh ${FS})
}

copy_binary() {
    BINARY="$1"
    if [ ! -d ${DIR}/fs/usr/local/bin/ ]; then
        doas mkdir -p ${DIR}/fs/usr/local/bin/
    fi

    log "Installing binary $binary"
    doas cp -v "${BIN_DIR}"/${BINARY} "${DIR}/fs/usr/local/bin/"
}

cleanup() {
    log "Unmounting image..."
    set +e
    doas /root/resflash/umount_resflash.sh 2> /dev/null || true
    set -e
}

main() {
    verify_user
    install_jq

    local ORIG_IMAGE=$(ls "${IMAGE_DIR}"/resflash-*-????????_????.img | sort | tail -n1)
    local ORIG_FS=$(ls "${IMAGE_DIR}"/resflash-*-????????_????.fs | sort | tail -n1)

    local ADDITIONAL_USER="$(cat host_config.json| jq -r '.global_config.additional_user')"
    local SSH_KEYS="$(cat host_config.json| jq -r '.global_config.ssh_pubkeys')"
    for host in $(cat host_config.json| jq -r '.host_config | keys[]'); do
        local HOST_IMAGE=${IMAGE_DIR}/resflash-cfgimg-${host}.$(echo ${ORIG_IMAGE}|awk -F . -safe '{ print $NF }')
        local HOST_FS=${IMAGE_DIR}/resflash-cfgimg-${host}.$(echo ${ORIG_FS}|awk -F . -safe '{ print $NF }')
        
        copy_image ${ORIG_IMAGE} ${HOST_IMAGE}
        mount_image ${HOST_IMAGE}

        add_user ${ADDITIONAL_USER}
        enable_doas
        move_root_home
        install_authorized_keys ${ADDITIONAL_USER} "${SSH_KEYS}"

        local domain=$(cat host_config.json| jq -r '.host_config["'$host'"].domain')
        set_hostname "${domain}"

        local pw_hash=$(cat host_config.json| jq -r '.host_config["'$host'"].root_password')
        if [ -z "${pw_hash}" ]; then
            log "No password defined for root, not setting password"
        else
            set_password root "${pw_hash}"
        fi

        for device in $(cat host_config.json | jq -r '.host_config["'$host'"].networks | keys[]'); do
            local config=$(cat host_config.json | jq -r '.host_config["'$host'"].networks["'$device'"]')
            setup_network "${device}" "${config}"
        done

        local binaries=$(cat host_config.json| jq -r '.host_config["'$host'"].binaries[]')
        for binary in $(echo $binaries); do
            copy_binary "$binary"
        done

        cleanup
        echo "Computing checksum..."
        doas cksum -a "${ALG:-sha512}" -h "${HOST_IMAGE}.cksum" "${HOST_IMAGE}"

        copy_image ${ORIG_FS} ${HOST_FS}
        mount_fs ${HOST_FS}
        local binaries=$(cat host_config.json| jq -r '.host_config["'$host'"].binaries[]')
        for binary in $(echo $binaries); do
            copy_binary "$binary"
        done
        cleanup

        echo "Computing checksum..."
        doas cksum -a "${ALG:-sha512}" -h "${HOST_FS}.cksum" "${HOST_FS}"
    done
}

main
