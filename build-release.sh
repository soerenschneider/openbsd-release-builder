#!/bin/sh

set -eu

THREADS=$(sysctl hw.ncpu | cut -d= -f2)
DESTDIR=/build
RELEASEDIR=${DESTDIR}/release
RELEASE_IMAGE_FILE=/home/release.img
IMAGE_SIZE=5000

RELEASE_FINAL_DIR=/release/sources

LOG_DIR="$(pwd)/logs/$(date +%Y%m%d)"
LOG_FILE="${LOG_DIR}/release.log"
USE_MFS="true"

VND_DEV="vnd4"

verify_user() {
    if [ $(id -u) -ne 0 ];  then
        echo "Must run as root"
        exit 1
    fi
}

create_image() {
    log "Creating image file"
    dd if=/dev/zero of="${RELEASE_IMAGE_FILE}" bs=1M count=${IMAGE_SIZE}  >> "${LOG_FILE}"
    vnconfig "${VND_DEV}" "${RELEASE_IMAGE_FILE}"
    newfs "${VND_DEV}"c
    log "Done creating image file"
}

mount_release_dir() {
    log "*** Mount dir to build release in"

    if [ ! -d "${DESTDIR}" ]; then
        mkdir "${DESTDIR}"
    fi

    if [ "${USE_MFS}" = "true" ]; then
        mount_mfs -o noperm -s 4000m swap ${DESTDIR}
    else
        mount -t ffs -o noperm /dev/"${VND_DEV}"c "${DESTDIR}"
    fi

    chmod 700 "${DESTDIR}"
    chown build "${DESTDIR}"

    if [ ! -d "${RELEASEDIR}" ]; then
        mkdir "${RELEASEDIR}"
    fi
    log "*** Finishing building release dir"
}

build_release() {
    export DESTDIR=${DESTDIR}
    export RELEASEDIR=${RELEASEDIR}
    
    log "*** make release"
    cd /usr/src/etc && make -j${THREADS} release >> "${LOG_FILE}"

    log "*** make checklist"
    # TODO: Investigate why checkflist returns != 0
    cd /usr/src/distrib/sets && sh checkflist || true >> "${LOG_FILE}"

    log "*** finish building release"
}

wind_down() {
    set +e
    log "Unmount ${DESTDIR}"
    umount "${DESTDIR}"
    
    if [ "${USE_MFS}" != "true" ]; then
        log "Unmount vnconfig"
        vnconfig -u "${VND_DEV}"
    fi
}

copy_release() {
    log "*** Copy release"
    if [ -d "${RELEASE_FINAL_DIR}" ]; then
        log "*** Deleting old content"
        rm -fr "${RELEASE_FINAL_DIR}"/* >> ${LOG_FILE}
    else
        mkdir -p "${RELEASE_FINAL_DIR}"
    fi
    cp -v "${RELEASEDIR}"/* "${RELEASE_FINAL_DIR}" >> "${LOG_FILE}"

    log "Delete ${RELEASE_IMAGE_FILE}"
    rm -f "${RELEASE_IMAGE_FILE}"
}

log() {
    echo "$(date +%H:%M:%S) $1" | tee -a "${LOG_FILE}"
}

log_initial() {
    if [ ! -d "${LOG_DIR}" ]; then
        mkdir -p "${LOG_DIR}"
    fi
    echo "$(date +%H:%M:%S) $1" | tee "${LOG_FILE}"
}

main() {
    verify_user
    log_initial "*** Start release"
    local START="${SECONDS}"
    #trap 'wind_down' ERR EXIT
    if [ "${USE_MFS}" != "true" ]; then
        create_image
    fi
    mount_release_dir
    build_release
    copy_release
    local END="${SECONDS}"
    log "Success, build time: $(((${END}-${START})/60)) min"
}

main
