#!/bin/sh

set -ue

MAJOR=$(uname -a | cut -d ' ' -f3 | cut -d '.' -f1)
MINOR=$(uname -a | cut -d ' ' -f3 | cut -d '.' -f2)
export CVSROOT=anoncvs@ftp.hostserver.de

BRANCH="OPENBSD_${MAJOR}_${MINOR}"

if [ $(id -u) -eq 0 ]; then
    echo "Must not run as root"
    exit 1
fi

if ! groups | grep wsrc; then
    doas user mod -G wsrc $(whoami)
    echo "please log out and back in"
    exit 1
fi

if [ ! -f /usr/src/Makefile ]; then
    echo "Cloning cvs from ${CVSROOT} for ${BRANCH}"
    cd /usr
    cvs -qd ${CVSROOT}:/cvs checkout -r"${BRANCH}" -P src
else
    echo "Fetching latest changes from ${CVSROOT}"
    cd /usr/src
    cvs -q up -Pd -r${BRANCH}
fi
