#!/bin/sh

set -ue

KERNEL=bsd.mp
RESFLASH_DIR=/root/resflash
SOURCES=/release/sources
EXTRACTED=/release/target
PACKAGES=node_exporter,neovim,curl,jq,wireguard-tools
SERIAL_SPEED=115200
IMAGE_LOCATION=/release/images
RESFLASH_SIZE_M=3814

BOOTSTRAP_VERSION=6.8
MIRROR=https://cdn.openbsd.org/pub/OpenBSD/${BOOTSTRAP_VERSION}/amd64

if [ $(id -u) -ne 0 ]; then
    echo Must be run as root
    exit 1
fi

if [ ${EXTRACTED} = '/' ]; then
    echo "Don't do that"
    exit 1
fi

fetch_resflash() {
    echo "Fetching resflash"
    if [ ! -d ${RESFLASH_DIR} ]; then
        pkg_add git
        git clone https://gitlab.com/bconway/resflash.git ${RESFLASH_DIR}
    else
        git -C ${RESFLASH_DIR} pull
    fi
}

bootstrap() {
    echo "Bootstrapping OpenBSD ${BOOTSTRAP_VERSION}"
    VERSION_SHORT=$(echo ${BOOTSTRAP_VERSION} | sed 's/\.//')
    curl -o ${SOURCES}/bsd "${MIRROR}/${KERNEL}"
    local ARCHIVES="base comp man"
    for archive in $ARCHIVES; do
        echo "Downloading ${MIRROR}/${archive}${VERSION_SHORT}.tgz"
        curl -o ${SOURCES}/${archive}${VERSION_SHORT}.tgz "${MIRROR}/${archive}${VERSION_SHORT}.tgz"
    done
    cd ${SOURCES}
    sha256 -c SHA256
    #curl -o ${SOURCES}/man${VERSION_SHORT}.tgz "${MIRROR}/man${VERSION_SHORT}.tgz"
}

extract_sources() {
    if [ ! -d ${EXTRACTED} ]; then
        mkdir -p ${EXTRACTED}
    else
        rm -rf ${EXTRACTED}/*
    fi

    echo "Verifying content"
    cd "${SOURCES}" && sha256 -c SHA256

    echo "Copying kernel"
    cp ${SOURCES}/${KERNEL} ${EXTRACTED}/bsd

    cd ${EXTRACTED}
    for archive in ${SOURCES}/*.tgz; do
        echo "Extracting ${archive}"
        tar zxfph "${archive}"
    done
    echo "Extracting var/sysmerge/etc.tgz"
    tar zxfph var/sysmerge/etc.tgz
    cd -
}

build_image() {
    if [ ! -d "${IMAGE_LOCATION}" ]; then
        mkdir -p "${IMAGE_LOCATION}"
    fi

    cd ${IMAGE_LOCATION}
    echo "Starting image build..."
    ${RESFLASH_DIR}/build_resflash.sh -s ${SERIAL_SPEED} --pkg_list "${PACKAGES}" "${RESFLASH_SIZE_M}" ${EXTRACTED}
    cd -
}

fetch_resflash
extract_sources
build_image
