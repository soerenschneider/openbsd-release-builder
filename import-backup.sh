#!/bin/sh

set -e

IMAGE_DIR=/release/images

verify_user() {
    if [ $(id -u) -ne 0 ]; then
        echo "Must be run as root"
        exit 1
    fi
}

import_backup() {
    if [ ! -f "${ARCHIVE}" ]; then
        echo "Archive ${ARCHIVE} does not exist"
        exit 1
    fi

    IMAGE=$(ls "${IMAGE_DIR}"/resflash-*.img | sort | tail -n1)
    DIR=$(/root/resflash/mount_resflash.sh ${IMAGE})
    trap cleanup EXIT ERR
    cd "${DIR}"
    openssl enc -salt -pbkdf2 -iter 10000 -aes256 -d -in "${ARCHIVE}" | tar pzxf -
}

cleanup() {
    echo "Cleaning up"
    /root/resflash/umount_resflash.sh
}

if [ $# -ne 1 ]; then
    echo "Need to supply image"
    exit 1
fi

ARCHIVE="$1"
verify_user
import_backup
