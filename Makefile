fetch:
	sh fetch-sources.sh

compile:
	doas sh compile.sh

release:
	doas sh build-release.sh

image:
	doas sh build-image.sh

additions:
	sh additions.sh

configure-image:
	sh configure-image.sh

all: fetch compile release image additions configure-image
