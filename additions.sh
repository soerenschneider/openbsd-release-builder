#!/bin/sh

set -eu

BIN_DIR=~/binaries
IMAGE_DIR=/release/images
HOME_DIR=/etc/home

verify_user() {
    if [ $(id -u) -eq 0 ]; then
        log "Must not be run as root"
        exit 1
    fi
}

create_binaries_dir() {
    if [ ! -d "${BIN_DIR}" ]; then
        mkdir "${BIN_DIR}"
    else
        rm -f "${BIN_DIR}"/*
    fi
}

build_pf_exporter() {
    install_go
    go install github.com/mischief/pf_exporter@latest
    cp ~/go/bin/pf_exporter "${BIN_DIR}"/
}

build_mtail() {
    install_go

    local mtail_dir=~/git/mtail
    if [ -d ${mtail_dir} ]; then
    log "Fetching latest changes for mtail"
    git -C ${mtail_dir} checkout main
    git -C ${mtail_dir} pull > /dev/null
    else
    log "Cloning mtail repository"
        git clone https://github.com/google/mtail ${mtail_dir} > /dev/null
    fi

    local version=$(git -C ${mtail_dir} tag | egrep ^v | sort -rV | head -n1)
    log "Checking out version ${version}"
    git -C ${mtail_dir} checkout "${version}" > /dev/null

    # unfortunately, the makefile is broken
    #make -C ${mtail_dir} test install
    cd ${mtail_dir} && go build -o "${BIN_DIR}/mtail" "${mtail_dir}/cmd/mtail/main.go"
    cd -
}

build_dyndns() {
    install_go
    if [ -d ~/git/dyndns ]; then
        log "Fetching latest changes for dyndns"
        git -C ~/git/dyndns checkout main
        git -C ~/git/dyndns pull
    else
        log "Cloning dyndns repository"
        git clone https://github.com/soerenschneider/dyndns ~/git/dyndns
    fi
    TAG=$(git -C ~/git/dyndns tag | sort -rV | head -n1)
    git -C ~/git/dyndns checkout "${TAG}"
    log "Building dyndns client ${TAG}"
    cd ~/git/dyndns
    local COMMIT_HASH="$(git rev-parse --short HEAD)"
    local MODULE="github.com/soerenschneider/dyndns"
    env CGO_ENABLED=0 go build -ldflags="-X '${MODULE}/internal.BuildVersion=${TAG}' -X '${MODULE}/internal.CommitHash=${COMMIT_HASH}'" -tags client -o dyndns-client cmd/client/client.go 2> /dev/null > /dev/null
    cp ~/git/dyndns/dyndns-client "${BIN_DIR}"
}

log() {
    echo "*** $1"
}

install_go() {
    if command -v go > /dev/null; then
    true
    else
    log "Installing go"
        doas pkg_add go
    fi
}

main() {
    verify_user
    create_binaries_dir
    build_pf_exporter
    build_mtail
    build_dyndns
}

main
