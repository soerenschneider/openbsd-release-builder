#!/bin/sh

set -eu

THREADS=$(sysctl hw.ncpu | cut -d= -f2)
KERNEL=GENERIC.MP
LOG_DIR="$(pwd)/logs/$(date +%Y%m%d)"

verify_user() {
    if [ $(id -u) -ne 0 ];  then
        echo "Must run as root"
        exit 1
    fi
}

verify_logging() {
    if [ ! -d "${LOG_DIR}" ]; then
        mkdir -p "${LOG_DIR}"
    fi
}

build_kernel() {
    log_initial "*** Start building kernel" kernel.log
    cd /sys/arch/$(machine)/compile/${KERNEL}
    
    local START="${SECONDS}"
    log "*** Make obj" kernel.log
    make -j${THREADS} obj >> "${LOG_DIR}/kernel.log"

    log "*** Make config" kernel.log
    make config >> "${LOG_DIR}/kernel.log"

    log "*** Make" kernel.log
    make -j${THREADS} >> "${LOG_DIR}/kernel.log"

    log "*** Make install" kernel.log
    make install >> "${LOG_DIR}/kernel.log"

    local END="${SECONDS}"
    log "Success, build time: $(((${END}-${START})/60)) min " kernel.log
}

build_base() {
    log_initial "*** Start building base" base.log
    
    cd /usr/src

    local START="${SECONDS}"
    log "Make obj" base.log
    make -j${THREADS} obj >> "${LOG_DIR}/base.log"
    
    log "make build" base.log
    make -j${THREADS} build >> "${LOG_DIR}/base.log"

    log "Sysmerge" base.log
    sysmerge -b || true >> "${LOG_DIR}/base.log"

    log "MAKEDEV" base.log
    cd /dev && ./MAKEDEV all >> "${LOG_DIR}/base.log"
    local END="${SECONDS}"

    log "Success, build time: $(((${END}-${START})/60)) min " base.log
}

log() {
    echo "$(date +%H:%M:%S) $1" | tee -a "${LOG_DIR}/$2"
}

log_initial() {
    if [ ! -d "${LOG_DIR}" ]; then
        mkdir -p "${LOG_DIR}"
    fi
    echo "$(date +%H:%M:%S) $1" | tee "${LOG_DIR}/$2"
}

verify_user
verify_logging
build_kernel
build_base
